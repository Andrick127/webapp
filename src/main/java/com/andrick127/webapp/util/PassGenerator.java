package com.andrick127.webapp.util;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

public class PassGenerator {

    public static void main(String ...args) {
        BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder(4);

        //El String es el password a encriptar.
        System.out.println(bCryptPasswordEncoder.encode("zaq12345"));

    }
}

