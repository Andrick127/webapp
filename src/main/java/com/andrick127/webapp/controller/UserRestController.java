package com.andrick127.webapp.controller;

import com.andrick127.webapp.entity.User;
import com.andrick127.webapp.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class UserRestController {

    @Autowired
    UserRepository userRepository;

    @GetMapping ("/users")
    public List<User> getAllUsers(){
        return (List<User>) userRepository.findAll();
    }

    @GetMapping("/user/{id}")
    public User getOne(@PathVariable(value = "id") Long id){
        return userRepository.findById(id).get();
    }

    @PostMapping("/user")
    public void add(User user){
        user.setStatus(true);
        userRepository.save(user);
    }

    @PutMapping("/user/{id}")
    public void update (User user,@PathVariable(value = "id") Long id){
        userRepository.findById(id).ifPresent((x)->{
            user.setId(id);
            user.setStatus(true);
            userRepository.save(user);
        });
    }
    @DeleteMapping("/user/{id}")
    public void delete (@PathVariable(value = "id") Long id){
        userRepository.deleteById(id);
    }
}
