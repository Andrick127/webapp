package com.andrick127.webapp.exception;

public class UsernameOrIdNotFound extends Exception{

    private static final long serialVersionUID = 6896443071444910373L;

    public UsernameOrIdNotFound(){
        super("Usuario o Id no encontrado");
    }

    public UsernameOrIdNotFound(String message){
        super(message);
    }
}
