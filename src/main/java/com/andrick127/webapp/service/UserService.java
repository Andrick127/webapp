package com.andrick127.webapp.service;

import com.andrick127.webapp.dto.ChangePasswordForm;
import com.andrick127.webapp.entity.User;
import com.andrick127.webapp.exception.UsernameOrIdNotFound;

public interface UserService {
    public Iterable<User> getAllUsers();
    public User createUser(User user) throws Exception;
    public User getUserById(Long id) throws Exception;
    public User updateUser(User user) throws Exception;
    public void deleteUser(Long id) throws UsernameOrIdNotFound;
    public User changeUserStatus(User user) throws Exception;
    public User changePassword(ChangePasswordForm form) throws Exception;
}
