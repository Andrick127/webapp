package com.andrick127.webapp.service;

import com.andrick127.webapp.dto.ChangePasswordForm;
import com.andrick127.webapp.entity.User;
import com.andrick127.webapp.exception.CustomFieldValidationException;
import com.andrick127.webapp.exception.UsernameOrIdNotFound;
import com.andrick127.webapp.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UserServiceImpl implements UserService{

    @Autowired
    UserRepository repository;
    @Autowired
    BCryptPasswordEncoder bCryptPasswordEncoder;

    @Override
    public Iterable<User> getAllUsers() {
        return repository.findAll();
    }

    private boolean checkUsernameAvailable(User user) throws Exception {
        Optional<User> userFound = repository.findByUsername(user.getUsername());
        if (userFound.isPresent()) {
            throw new CustomFieldValidationException("Username no disponible", "username");
        }
        return true;
    }

    private boolean checkPasswordValid(User user) throws Exception {
        if(user.getPasswordConfirm()==null || user.getPasswordConfirm().isEmpty()){
            throw new CustomFieldValidationException("Debes confirmar la contraseña", "passwordConfirm");
        }
        if ( !user.getPassword().equals(user.getPasswordConfirm())) {
            throw new CustomFieldValidationException("Las contraseñas no coinciden", "password");
        }
        return true;
    }


    @Override
    public User createUser(User user) throws Exception {
        if (checkUsernameAvailable(user) && checkPasswordValid(user)) {
            user.setStatus(true);
            String encodedPassword = bCryptPasswordEncoder.encode(user.getPassword());
            user.setPassword(encodedPassword);
            user = repository.save(user);
        }
        return user;
    }

    @Override
    public User getUserById(Long id) throws UsernameOrIdNotFound {
        return repository.findById(id).orElseThrow(() -> new UsernameOrIdNotFound("El Id del usuario no existe."));
    }

    @Override
    public User updateUser(User fromUser) throws Exception {
        User toUser = getUserById(fromUser.getId());
        mapUser(fromUser, toUser);
        toUser.setStatus(true);
        return repository.save(toUser);

    }
    /**
     * Se mapeoa el objeto user excepto por password
     * @param from
     * @param to
     */
    protected void mapUser(User from, User to){
        to.setName(from.getName());
        to.setLastName(from.getLastName());
        to.setEmail(from.getEmail());
        to.setUsername(from.getUsername());
        to.setRoles(from.getRoles());
        to.setStatus(from.getStatus());
    }

    @Override
    @PreAuthorize("hasAnyRole('ROLE_ADMIN')")
    public void deleteUser(Long id) throws UsernameOrIdNotFound {
        User user = getUserById(id);
        repository.delete(user);
    }

    @Override
    public User changePassword(ChangePasswordForm form) throws Exception {
        User user = getUserById(form.getId());

        if ( !isLoggedUserADMIN() && !user.getPassword().equals(form.getCurrentPassword())) {
            throw new Exception("verifique la contraseña actual.");
        }
        if( user.getPassword().equals(form.getNewPassword())) {
            throw new Exception("La contraseña nueva no puede ser igual a la anterior\n");
        }
        if( !form.getNewPassword().equals(form.getConfirmPassword())) {
            throw new Exception("La nueva contraseña y la verificacion no coinciden\n");
        }

        String encodePassword = bCryptPasswordEncoder.encode(form.getNewPassword());
        user.setPassword(encodePassword);
        user.setStatus(true);
        return repository.save(user);
    }  

    private boolean isLoggedUserADMIN() {
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		
		UserDetails loggedUser = null;
		Object roles = null;

		//Verificar que ese objeto traido de sesion es el usuario
		if (principal instanceof UserDetails) {
			loggedUser = (UserDetails) principal;

			roles = loggedUser.getAuthorities().stream()
					.filter(x -> "ROLE_ADMIN".equals(x.getAuthority())).findFirst()
					.orElse(null); 
		}
		return roles != null ? true : false;
    }

    private User getLoggedUser() throws Exception {
		//Obtener el usuario logeado
		Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		
		UserDetails loggedUser = null;

		//Verificar que ese objeto traido de sesion es el usuario
		if (principal instanceof UserDetails) {
			loggedUser = (UserDetails) principal;
		}
		
		User myUser = repository
				.findByUsername(loggedUser.getUsername()).orElseThrow(() -> new Exception("Error obteniendo el usuario logeado desde la sesion."));
		
		return myUser;
	}

    @Override
    public User changeUserStatus(User userFrom) throws Exception {
        User userTo= getUserById(userFrom.getId());
        mapUser(userFrom,userTo);
        if(userTo.getStatus()){
            userTo.setStatus(false);
        }
        if(!userTo.getStatus()){
            userTo.setStatus(true);
        }
        return repository.save(userTo);
    }
}
