package com.andrick127.webapp.service;

import com.andrick127.webapp.entity.Role;
import com.andrick127.webapp.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashSet;
import java.util.Set;

@Service
@Transactional
public class UserDetailsServiceImpl implements UserDetailsService {

    @Autowired
    UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        com.andrick127.webapp.entity.User appUser = userRepository.findByUsername(username).orElseThrow(()->new UsernameNotFoundException("Login username invalido"));

        Set grantList = new HashSet();
        //Crear lista de roles/accesos de los usuarios
        for (Role role:appUser.getRoles()){
            GrantedAuthority grantedAuthority = new SimpleGrantedAuthority(role.getDescription());
            grantList.add(grantedAuthority);
        }
        UserDetails user = (UserDetails) new User(username,appUser.getPassword(),grantList);
        return user;
    }
}
